<?php

/**
 * @file
 * Include file for services_basic module.
 */

/**
 * Authenticates a call using Basic HTTP authentication to verify the request.
 *
 * @param array $settings
  *  The settings for the authentication module.
 * @param array $method
 *  The method that's being called
 * @param array $args
 *  The arguments that are being used to call the method
 * @return void|string
 *  Returns nothing, or a error message if authentication fails
 */
function _services_basichttpauth_authenticate_call($settings, $method, $args) {
  global $user;
  if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $name = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW'];

    // if the username is an email address
    if (strstr($name, "@")) {
      $tmp_user = user_load(array("mail" => $name));
      $name = $tmp_user->name;
    }

    // Note the short circuit preventing authentication of blocked users.
    if (user_is_blocked($name) || !($uid = user_authenticate(array('name'=>$name, 'pass'=>$pass)))) {
      $site_name = trim(variable_get('site_name', 'Drupal'));
      $realm = mime_header_encode($site_name);
      drupal_set_header('Status: 401 Unauthorized');
      drupal_set_header('WWW-Authenticate: Basic realm="' . $realm .'"');
    }
    else {
      $user = user_load($uid->uid);
    }
  }
}

function _services_basichttpauth_controller_settings() {
  return array();
}

